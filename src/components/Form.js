import { useState } from 'react'
import axios from 'axios'

import React, { Component } from 'react'

export default class form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userId: "",
            title: "",
            body: ""
        }
    }

    handle = (e) => {
        this.setState({ [e.target.name]: [e.target.value] })
    }


submitHandler = (e) => {
    e.preventDefault()
    console.log(this.state);

    axios.post("https://jsonplaceholder.typicode.com/posts", this.state).then(response=>{
        console.log(response)
    }).catch(error=>{
        console.log(error)
    })
}

render() {
    const { id, title, body } = this.state
    return (
        <div>
            <form onSubmit={this.submitHandler}>
                <input type="text" name="userId" value={id} placeholder="userId" onChange={this.handle} />
                <input type="text" name="title" value={title} placeholder="Title" onChange={this.handle} />
                <input type="text" name="body" value={body} placeholder="Body" onChange={this.handle} />
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}
}

